# Docker for ARMv7 Only

[![pipeline status](https://gitlab.com/klud/docker-in-docker/badges/master/pipeline.svg)](https://gitlab.com/klud/docker-in-docker/commits/master)
 [![Docker Pulls](https://img.shields.io/docker/pulls/klud/docker.svg)](https://hub.docker.com/r/klud/docker/)

[![](https://images.microbadger.com/badges/image/klud/docker.svg)](https://microbadger.com/images/klud/docker "Get your own image badge on microbadger.com")
[![](https://images.microbadger.com/badges/version/klud/docker.svg)](https://microbadger.com/images/klud/docker "Get your own version badge on microbadger.com")

[![](https://images.microbadger.com/badges/image/klud/docker:dind.svg)](https://microbadger.com/images/klud/docker:dind "Get your own image badge on microbadger.com")
[![](https://images.microbadger.com/badges/version/klud/docker:dind.svg)](https://microbadger.com/images/klud/docker:dind "Get your own version badge on microbadger.com")

[![](https://images.microbadger.com/badges/image/klud/docker:git.svg)](https://microbadger.com/images/klud/docker:git "Get your own image badge on microbadger.com")
[![](https://images.microbadger.com/badges/version/klud/docker:git.svg)](https://microbadger.com/images/klud/docker:git "Get your own version badge on microbadger.com")

This image bundles the ARMv7 binary

As of Docker v18.02 the official Docker image supports ARMv6 (a.k.a armel)

```sh
$ manifest-tool inspect docker
Name:   docker (Type: application/vnd.docker.distribution.manifest.list.v2+json)
Digest: sha256:bf51c000f7b68dc1cb940bbdae15345ecccd2c73401b777ea74b14866439b6b6
 * Contains 5 manifest references:
1    Mfst Type: application/vnd.docker.distribution.manifest.v2+json
1       Digest: sha256:9a5257debf7af0bb8bc80e00635ee81244fabe5b281a7c580fab3f1b51d113c6
1  Mfst Length: 1571
1     Platform:
1           -      OS: linux
1           - OS Vers: 
1           - OS Feat: []
1           -    Arch: amd64
1           - Variant: 
1           - Feature: 
1     # Layers: 6
         layer 1: digest = sha256:ff3a5c916c92643ff77519ffa742d3ec61b7f591b6b7504599d95a4a41134e28
         layer 2: digest = sha256:1a649ea86bcaa0acdca25d22520d9d7b6d6373c3e4a385a21b48c2757e8ec6ac
         layer 3: digest = sha256:ce35f4d5f86ae68ae9e5cb6590ecdcca2ae5257cbedb85fe4bfd2efa05f73b2f
         layer 4: digest = sha256:d00fbe3653a144d0c5d60ec84930fd4ea793cadd5d114ab5d330879e722360e2
         layer 5: digest = sha256:e14031ffc7fbc6a9e38641de13545fa1c0b064d6798b9b72bbd77609aa0d4fc2
         layer 6: digest = sha256:b6f2ec0da0a67ef4c3e5c32e06110ed381bd2c546aa6d095c02c13cf8fef2a36

2    Mfst Type: application/vnd.docker.distribution.manifest.v2+json
2       Digest: sha256:848aca3e46905cc73508a345ecc7b0c17487a57a6cd6e3bc504dd1b45210d243
2  Mfst Length: 1778
2     Platform:
2           -      OS: linux
2           - OS Vers: 
2           - OS Feat: []
2           -    Arch: arm
2           - Variant: v6
2           - Feature: 
2     # Layers: 7
         layer 1: digest = sha256:95d54dd4bdadebb53f9b91b25aa7dc5fcb83c534eb1d196eb0814aa1e16f3db2
         layer 2: digest = sha256:5993b3593c77413be85d318297ad8313b945069768a7e454d487fd47fa4b4343
         layer 3: digest = sha256:1d39d398468dbd6293edae0e1681356b774ec38786684c38474357d28cd7302c
         layer 4: digest = sha256:1d10527b062766eb7d518754d794ba65aaf8d138e7af412daf172e9b25ce66c5
         layer 5: digest = sha256:0f94f64578d9bd0e79ef5f1005fcd7bca505f4457f08ae32ed7d4d633b565d96
         layer 6: digest = sha256:165a5a791819d4007890d7f66a5b56aff0deaf2544e8025af051c6402b27862f
         layer 7: digest = sha256:70b89bea87f9b7b11735c5f5e2b92e9afa07c98172e032ec0c5ae5ec64700a9e

3    Mfst Type: application/vnd.docker.distribution.manifest.v2+json
3       Digest: sha256:db619e4474b9aa01abf3dcf630806c2359afe20fecefa8c04c9f61f4ca5a014d
3  Mfst Length: 1778
3     Platform:
3           -      OS: linux
3           - OS Vers: 
3           - OS Feat: []
3           -    Arch: arm64
3           - Variant: v8
3           - Feature: 
3     # Layers: 7
         layer 1: digest = sha256:b78042c299ad99d1e646b18762d4bc22a84c4f88e5bb491ea6293a10f53ddf79
         layer 2: digest = sha256:6fd45b97b6c2a3ac869ae5c99e087e97bc29714b165180e06f0c9116f400f2dd
         layer 3: digest = sha256:61e0b1d8a4679a04839bcedd494b39879dc202e375f6f74d26f6bd80edff0363
         layer 4: digest = sha256:226bcca23678813260f44b3560835eb92c99b7a375b8f4dd0e264c06496a201d
         layer 5: digest = sha256:4f665c85e58ab1ce422abf4e6661e448e030019fa5528fb0b4a5f10d505ceb80
         layer 6: digest = sha256:8082ee559458cf0a6345c0c0153adaa1903f4516b9b7670e7c253eb0a4fc3269
         layer 7: digest = sha256:27f1685c5d17c00a7af22712a4cbdf00d3e8249a09f7360ddb62236608893189

4    Mfst Type: application/vnd.docker.distribution.manifest.v2+json
4       Digest: sha256:5f1efdd3e9d730dddf097b22107b6750efdf0483a5023c7cef46b7bfee5fcd5b
4  Mfst Length: 1778
4     Platform:
4           -      OS: linux
4           - OS Vers: 
4           - OS Feat: []
4           -    Arch: ppc64le
4           - Variant: 
4           - Feature: 
4     # Layers: 7
         layer 1: digest = sha256:0da653ea85b50d280ec56ca2eafb7e8b37590630356e043fa9ff162d55732a23
         layer 2: digest = sha256:9fd90b777cc38b5b6ca1b2407e647fdc22ef31b57ef98e924e7e0635adffc385
         layer 3: digest = sha256:2fe230e03b98ad6c09f4e89c524a8f39e17835ba689b3035c55bbbef18956540
         layer 4: digest = sha256:f6307b533b9be0ac40a1422292494cdd1f448afb34ba047614c035d8ab361452
         layer 5: digest = sha256:7eee55054dfdfdad93e0338d10d014bed6b11c34223671af4de273cf9c88c6fc
         layer 6: digest = sha256:e3262c54805ee155ed58c61bd4fc082372f776676ae70117e7b88d78f48389b3
         layer 7: digest = sha256:1256a79d185863fd8a94c556aef66c4e1b342f357131aa09e098d9c8bed31645

5    Mfst Type: application/vnd.docker.distribution.manifest.v2+json
5       Digest: sha256:e31a32b72061bb8598e6b931b9bb70a12973ebf07caa66c541229b00f707aa40
5  Mfst Length: 1778
5     Platform:
5           -      OS: linux
5           - OS Vers: 
5           - OS Feat: []
5           -    Arch: s390x
5           - Variant: 
5           - Feature: 
5     # Layers: 7
         layer 1: digest = sha256:11e7bc85614a236b32043d147930fd2bc9055af8642fe30e5e56142590572b0e
         layer 2: digest = sha256:3f825cbb729285f1fe2a0cd1d4d36897e3fe2191c5ee044ce11a5d301dc64a34
         layer 3: digest = sha256:8e086971261bceaf8aea6aa9962223fd5f1c0876f30d440dca2edce64bb2e6ea
         layer 4: digest = sha256:b94801dbdd0e977fe92249d99be1d017b2b930177b6d3dd44105722b0233438b
         layer 5: digest = sha256:3c9ce7e683c54c27ea56fbcf72671deac91f50d449f79fbed28329a0c09ae0f0
         layer 6: digest = sha256:7e17c18fcd198df3fd50da7fcd01dfcf02363f47c10145fc6a22ae9810835df7
         layer 7: digest = sha256:66f2457d23943448ec864b358845299c167c722d4239b2e6ef1ff462b72d5e51
```
